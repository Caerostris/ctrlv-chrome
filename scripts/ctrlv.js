/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

var copy = false;

document.addEventListener('keydown', function(e) {
	if(e.keyIdentifier == 'U+0043' && e.ctrlKey && e.altKey) {
		copy = true;
	}
});

document.addEventListener('keyup', function(e) {
	if(e.keyIdentifier == 'U+0043') {
		if(copy) {
			chrome.runtime.sendMessage({text: window.getSelection().toString()});
			copy = false;
		}
	}
});
