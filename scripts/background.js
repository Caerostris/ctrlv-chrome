/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

function ctrlv_paste(text, callback) {
	var xmlhttp;
	if(window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.open('POST', 'https://ctrlv.zsi.li/submit', true);
	xmlhttp.setRequestHeader('Content-type', 'application/json;charset=UTF-8');
	xmlhttp.send(JSON.stringify({ syntax: 'Plain', expires: '1 Month', content: text, redirect: 'false'}));
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4) {
			if(xmlhttp.status == 200) {
				if(xmlhttp.responseText == 'invalid parameters') {
					callback(true, xmlhttp.responseText);
				}
				else {
					callback(false, xmlhttp.responseText);
				}
			}
			else {
				callback(true, xmlhttp.responseText);
			}
		}
	};
}

var id = 0;
function notify(title, text) {
	var options = {
		iconUrl: 'https://www.google.com/favicon.ico',
		type: 'basic',
		title: title,
		message: text,
		priority: 1,
	};
	chrome.notifications.create('ctrlv_id'+id++, options, function(id) {
		setTimeout(function() {
			chrome.notifications.clear(id, function(id) {});
		}, 3000);
	});
}

function copy(text) {
	var input = document.getElementById("text");
	if(input === undefined) {
		return;
	}

	input.value = text;
	input.select();
	document.execCommand('copy', false, null);
	notify('CtrlV', 'Link copied to clipboard! :)');
}

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		ctrlv_paste(request.text, function(err, link) {
			if(!err) {
				copy(link);
			}
		});
	}
);
